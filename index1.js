
console.log("============ jawaban soal no 1================\n")

const luas_persegi_panjang=(p,l)=>{

    return p*l

}

const keliling_persegi_panjang=(p,l)=>{
    return 2*(p+l)
}

console.log(luas_persegi_panjang(8,4))
console.log(keliling_persegi_panjang(8,4))


console.log("============ jawaban soal no 2 ================\n")

const newFunction = (firstName, lastName)=>{
    return {
       firstName,
      lastName,
      fullName: ()=>{
        console.log(`${firstName}  ${lastName}`)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 


  console.log("============ jawaban soal no 3 ================\n")


  const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

  const {firstName,lastName,address,hobby}=newObject

// Driver code
console.log(`${firstName} ${lastName} ${address} ${hobby}`)


console.log("============ jawaban soal no 4 ================\n")


const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined=[...west,...east]
//Driver Code
console.log(combined)

console.log("============ jawaban soal no 5 ================\n")


const planet = "earth" 
const view = "glass" 
const before = `Lorem ${view} dolor sit amet,consectetur adipiscing elit,${planet}` 

console.log(before);
